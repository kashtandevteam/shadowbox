#!/bin/bash

NAME="prod"
HOMEDIR=/home/jacks/api.shadowbox.solutions
VENVDIR=$HOMEDIR/.env
PROJECTDIR=$HOMEDIR
SOCKFILE=/var/run/api/pid.sock
LOGFILE=$HOMEDIR/etc/logs/gunicorn.log
USER=jacks
NUM_WORKERS=3
MAX_REQUESTS=1
TIMEOUT=30

echo "Starting $NAME as `whoami`"

cd $PROJECTDIR
source $VENVDIR/bin/activate

export SETTINGS_MODULE=conf.settings.prod

exec $VENVDIR/bin/gunicorn manage:app \
  --name $NAME \
  --env SETTINGS_MODULE=conf.settings.prod \
  --workers $NUM_WORKERS \
  --max-requests $MAX_REQUESTS \
  --user=$USER\
  --bind=unix:$SOCKFILE \
  --access-logfile=$LOGFILE \
  --log-file=$LOGFILE \
  --log-level=warning \
  --timeout=$TIMEOUT