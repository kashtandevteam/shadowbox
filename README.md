### SHADOW BOX API
** API VERSION : 2 **

Current API was build on python falcon framework.
Bellow was given short information about libraries, which was installed with short descriptions, architecture of current service and little instruction for server configuration.

### Architecture
Current architecture consist of app, conf, etc and tests sections.

** app **
Is main service section that include middleware, models, resources, statics, templates, additional functionality and allow routes creation

Current section consist of next dependencies:

1. Middleware ( middlewares )
Current section allow checking authorization is system, checking requested header parameters and allow change response in json format.

2. Models ( models )
That section allow connection to database tables and allow getting, updating or inserting information to that tables

3. Resources ( resources )
That section allow processing all request to system, validate requested parameters and give appropriate response.

4. Static ( static )
That section allow storing css/js/images/fonts and uploaded files in sistem

5. Templates ( templates )
That section allow storing html( view ) files for pdf convert request only

4. Utils section ( utils )
Current section allow to get additional functionality for current service like returning today in timestamp

** conf **
Current directory allow getting custom configurations for database details.
Also allow adding additional config settings and divided by environments

** etc **
Current directory is need for getting custom server configurations ( nginx, supervisor )
Also has api documentation and directory with log issues from services like: nginx, supervisor and gunicorn

It consists of next directories:

1. API Section ( api )
In this section you can get api documentation

2. Deploy Section ( deploy )
In this section can be found configuration files for server services like nginx, supervisor

3. Logs Section ( logs )
This section allow to get all logs from server services like nginx, supervisor and gunicorn.

** tests **
Current section allow running unit test for models and resources

### API Documentation
Documentation for current API can be found in directory /etc/api/v2.md

### API server configuration
API requires next services :

 ** nginx **
   Configuration file located at directory /etc/deploy/{environment}/nginx-dev

 ** supervisor **
  Configuration file located at directory /etc/deploy/{environment}/supervisor-dev

 ** virtualenv **
    Please run ** virtualenv -p python3 .env **
    After that please run command in root user ** easy_install pip **
    After installing we need be activate our virtualenv and install all need requirements from requirements.txt file, for doing that please
    1) run ** source .env/bin/activate **
    2) then ** pip install -r requirements.txt **

  ** gunicorn **
    Please do not forget to make configuration in gunicorn file, which located at /etc/deploy/{environment}/gunicorn-run.sh directory

Restart nginx and supervisor and enjoy

### Main Sections in API and short description of this sections
Current API has 4 main sections and they are :

    ** Auth **
        Authorisation can be done through public key and mac address
        Current method will return temporary token and this token will need for generating token and that will allow access for other API requests.
        This token should be added in every requests.

    ** Log out **
        Closing current session for security reasons

    ** Registration of new device **
        Allows register new device without using authorization token.
        Current method require only 2 params: { public key } and { mac address }
        I.E Public key should be generated in admin panel

    ** PDF conversion **
        Conversion option to pdf file and from pdf to html file respectively due to requested data

### API installation of libraries and short description
API consist of the list of next libraries, which should be installed by running command **pip install -r requirements.txt **

The list of libraries and short descriptions of them :

 ** arrow **           Current library helps for creating human-friendly, formatting and converting dates, times, and timestamps

 ** docutils **        System for processing documentation into useful formats, such as HTML, XML, and LaTeX.

 ** jmespath **        Allows to declaratively specify how to extract elements from a JSON document )

 ** python-dateutil ** The dateutil module provides powerful extensions to the datetime

 ** Cerberus **        Cerberus provides powerful yet simple and lightweight data validation functionality out of the box and is designed to be easily extensible, allowing for custom validation

 ** click **           Click is a Python package for creating beautiful command line interfaces in a composable way with as little code as necessary

 ** Cython **          Cython is an optimising static compiler for both the Python programming language and the extended Cython programming language (based on Pyrex). It makes writing C extensions for Python as easy as Python itself

 ** falcon **          Python web API framework for building high-performance microservices, app backends, and higher-level frameworks

 ** futures **         Current module provides a high-level interface for asynchronously executing callables.

 ** gunicorn **        The Gunicorn server is broadly compatible with various web frameworks, simply implemented, light on server resources, and fairly speedy.

 ** pymysql **         This package contains a pure-Python MySQL client library. The goal of PyMySQL is to be a drop-in replacement for MySQLdb and work on CPython, PyPy and IronPython.

 ** pony **            Pony translates such queries into SQL and executes them in the database in the most efficient way.

 ** nose **            Nose extends the test loading and running features of unittest

 ** passlib **         Passlib is a password hashing library for Python

 ** PyYAML **          YAML is a data serialization format designed for human readability and interaction with scripting languages. PyYAML is a YAML parser and emitter for Python.

 ** requests **        Requests is the only Non-GMO HTTP library for Python, safe for human consumption

 ** six **             It provides utility functions for smoothing over the differences between the Python versions with the goal of writing Python code that is compatible on both Python versions

 ** bcrypt **          Modern password hashing for software and servers

** PyPDF2 **           A Pure-Python library built as a PDF toolkit. It is capable of: extracting document information (title, author, …), splitting documents page by page, merging documents page by page, cropping pages, merging multiple pages into a single page, encrypting and decrypting PDF files and more!

** python-barcode **   This library provides a simple way to create barcodes using only the Python standard lib. The barcodes are created as SVG objects.

** pdfkit **           This is adapted version of ruby PDFKit library, so big thanks to them!

** jinja2 **           Jinja2 is a full featured template engine for Python. It has full unicode support, an optional integrated sandboxed execution environment, widely used and BSD licensed.

** python-hl7 **       Is a simple library for parsing messages of Health Level 7 (HL7) version 2.x into Python objects

** bs4 **              Beautiful Soup is a Python library for pulling data out of HTML and XML files. It works with your favorite parser to provide idiomatic ways of navigating, searching, and modifying the parse tree. It commonly saves programmers hours or days of work.