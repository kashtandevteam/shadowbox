import os

from .base import *

DB_NAME     = ''
DB_HOST     = 'localhost'
DB_USER     = 'root'
DB_PASSWORD = ''
DB_TYPE      = 'mysql'
SSL_OPTIONS  = {
    'ssl-mode': 'preferred'
}

CORS_ORIGIN  = '*'
STATIC_DIR   = os.path.abspath( os.path.join( os.path.dirname( __file__ ), '..', '..', 'app/static' ) )
TEMPLATE_DIR = os.path.abspath( os.path.join( os.path.dirname( __file__ ), '..', '..', 'app/templates' ) )