import importlib, os

class SettingsWrapper( object ):
    """
    Attributes:
        API_VERSION (str)
        DB_NAME (str)
        DB_HOST (str)
        DB_PORT (int)
        DB_USER (str)
        DB_PASSWORD (str)
        SSL_OPTIONS (list)
        STATIC_DIR (str)
        TEMPLATE_DIR (str)
    """

    def __init__( self ):
        """
        METHOD     : __init__
        Description: Current method is
        used to import all available
        settings attributes
        """

        self.defaults        = {}
        self.settings_module = os.environ.get(
            'SETTINGS_MODULE',
            'conf.settings'
        )

        if self.settings_module:

            self.settings = importlib.import_module(
                self.settings_module
            )

    def __getattr__( self, key ):
        """
        METHOD     : __getattr__
        Description: Current method is
        used to check settings attribute
        existence
        """

        value = self.defaults.get(
            key,
            'unknown setting'
        )

        if hasattr( self.settings, key ):

            value = getattr(
                self.settings, key
            )

        if value == 'unknown setting':

            raise AttributeError(
                'No setting for "{}".' . format( key )
            )

        return value

settings = SettingsWrapper()