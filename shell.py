import code

from conf import settings
from pony.orm import set_sql_debug

# The list of available database tables
from .models.base import db
from .models.devices import Devices
from .models.tokens import Tokens

# Database Connection
db.bind(
    provider = settings.DB_TYPE,
    host     = settings.DB_HOST,
    user     = settings.DB_USER,
    passwd   = settings.DB_PASSWORD,
    db       = settings.DB_NAME
)

db.generate_mapping(
    create_tables = False
)

set_sql_debug( True )

imported_objects = globals().copy()
imported_objects.update( locals() )

console = code.interact( local = imported_objects )