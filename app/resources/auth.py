import falcon

from falcon import HTTPUnauthorized

from pony.orm import db_session

from datetime import datetime

from app.models.devices import Devices
from app.models.tokens import Tokens

from .base import BaseResource

class AuthResource( BaseResource ) :
    """
    RESOURCE   : AuthResource
    Description: Current resource is
    used to check device existence
    due to requested public key and
    mac address
    """

    require_auth      = False
    validation_scheme = {
        'public_key'   : {
            'type'    : 'string',
            'required': True
        },
        'mac_address': {
            'type'    : 'string',
            'required': True
        },
    }

    @db_session
    def on_post( self, req, resp ):
        """
        METHOD     : on_post
        Description: Current method allow
        checking device existence due to
        requested public key and mac address
        and generating temp token for
        authorizations
        """

        data = self.validate( req )

        try:

            device = Devices.select(
                lambda device: device.public_key == data[ 'public_key' ]
                               and device.mac_address == data[ 'mac_address' ]
                               and device.is_new == 0
            ).first()

            if device.private_key:

                token = Devices.generate_token( {
                    'public_key' : device.public_key,
                    'private_key': device.private_key,
                    'mac_address': device.mac_address
                } )

            else:

                raise HTTPUnauthorized(
                    code        = 401,
                    title       = 'Authentication failed',
                    description = 'Please check your parrams and try again'
                )

        except Exception:

            raise HTTPUnauthorized(
                code        = 401,
                title       = 'Authentication failed',
                description = 'Please check your parrams and try again'
            )

        req.context[ 'result' ] = {
            'status'         : 'success',
            'code'           : 200,
            'temporary_token': token[ 'time' ]
        }

class LogoutResource ( BaseResource ):
    """
    RESOURCE   : LogoutResource
    Description: Current resource is
    used to close current session by
    requested token in header
    """

    @db_session
    def on_get( self, req, resp ):
        """
        METHOD     : on_get
        Description: Current method closing
        current session by requested token
        in header
        """

        try:

            token = Tokens.select( lambda tokens: tokens.mac_address == self.device.mac_address ).first()

            if token is not None:

                current_token = Tokens[ token.id ]

                current_token.save( {
                    'expiration_date' : datetime.utcnow()
                } )

        except Exception:

            raise falcon.HTTPUnauthorized(
                code        = 401,
                title       = 'Authentication failed',
                description = 'Your session expired. Please re-login again'
            )

        req.context[ 'result' ] = {
            'status' : 'success',
            'code'   : 200,
            'message': 'You was log out successfully'
        }