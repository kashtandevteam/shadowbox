import falcon, os, pdfkit, re, json

from lxml import etree

from base64 import b64encode

from pony.orm import *

from .base import BaseResource

from conf import settings

class HL7Convert( BaseResource ):
    """
    RESOURCE   : HL7Convert
    Description: Current resource allow
    hl7 converting due to requested data
    """

    validation_scheme = {
        'emr': {
            'type'    : 'string',
            'required': True
        },
        'params': {
            'required': True
        },
        'version': {
            'required': True
        }
    }

    @db_session
    def on_post( self, req, resp ):
        """
        METHOD     : on_post
        Description: Current method allow
        hl7 converting due to requested data
        and return response with file content
        in base 64 format and with appropriate
        message
        """

        data = self.validate( req )

        try:

            patient_data      = etree.Element( "Patient", xmlns = "http://hl7.org/fhir" )
            patient_data.append( etree.Element( "id", value = "pat1" ) )
            patient_data.append( etree.Element( "active", value = "true" ) )
            patient_data.append( etree.Element( "gender", value = "male" ) )
            patient_data.SubElement( "birthDate", value = "1974-12-25" )

            patient_text = patient_data.SubElement( "text" )
            patient_text.append( etree.Element( "status", value = "generated" ) )

            identifier = patient_data.SubElement( "identifier" )
            identifier.append( etree.Element( "use", value = "usual" ) )
            identifier.append( etree.Element( "system", value = "urn:oid:0.1.2.3.4.5.6.7" ) )
            identifier.append( etree.Element( "value", value = "654321" ) )

            type   = identifier.SubElement( 'type' )
            coding = type.SubElement( 'coding' )
            coding.append( etree.Element( "system", value = "http://hl7.org/fhir/v2/0203" ) )
            coding.append( etree.Element( "code", value = "MR" ) )

            name = patient_data.SubElement( "name" )
            name.append( etree.Element( "use", value = "official" ) )
            name.append( etree.Element( "family", value = "Chalmers" ) )
            name.append( etree.Element( "given", value = "Peter" ) )
            name.append( etree.Element( "given", value = "James" ) )

            phone = patient_data.SubElement( "telecom" )
            phone.append( etree.Element( "system", value = "phone" ) )
            phone.append( etree.Element( "value", value = "(03) 5555 6473" ) )
            phone.append( etree.Element( "use", value = "work" ) )
            phone.append( etree.Element( "rank", value = "1" ) )

            mobile = patient_data.SubElement( "telecom" )
            mobile.append( etree.Element( "system", value = "phone" ) )
            mobile.append( etree.Element( "value", value = "(03) 3410 5613" ) )
            mobile.append( etree.Element( "use", value = "mobile" ) )
            mobile.append( etree.Element( "rank", value = "2" ) )

            address = patient_data.SubElement( "address" )
            address.append( etree.Element( "use", value = "home" ) )
            address.append( etree.Element( "type", value = "both" ) )
            address.append( etree.Element( "text", value = "534 Erewhon St PeasantVille, Rainbow, Vic  3999" ) )
            address.append( etree.Element( "line", value = "534 Erewhon St" ) )
            address.append( etree.Element( "city", value = "PleasantVille" ) )
            address.append( etree.Element( "district", value = "Rainbow" ) )
            address.append( etree.Element( "state", value = "Vic" ) )
            address.append( etree.Element( "postalCode", value = "3999" ) )

            managingOrganization = patient_data.SubElement( "managingOrganization" )
            managingOrganization.append( etree.Element( "reference", value = "Organization/1" ) )


        except:

            raise falcon.HTTPNotFound(
                code        = 400,
                title       = 'Validation Error',
                description = 'Please check your params and try again'
            )

        req.context[ "result" ] = {
            'status' : 'success',
            'code'   : 200,
            'params' : json.dumps ( {
                'file_content' : ''
            }, indent = 2 )
        }