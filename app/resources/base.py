import falcon, os, jinja2

from cerberus import Validator

from conf import settings

class BaseResource( object ):
    """
    RESOURCE   : BaseResource
    Description: Current resource allow
    check authorization, parameters
    validation and response in list
    format
    """

    require_auth      = True
    validation_scheme = None
    device            = None

    def check_auth( self, req ):
        """
        METHOD     : check_auth
        Description: Current method checking
        device authorization
        """

        if not self.device:

            raise falcon.HTTPUnauthorized(
                code        = 401,
                title       = 'Authentication failed',
                description = 'Please check your param and try again'
            )

    def validate( self, req, is_list = False ):
        """
        METHOD     : validate
        Description: Current method allow
        requested param validations
        """

        if is_list:
            data = req
        else:
            data = req.context[ 'json' ]

        if self.validation_scheme:

            if data is None:
                data = {}

            v = Validator()
            v.validate( data, self.validation_scheme )

            if v.errors:
                raise falcon.HTTPBadRequest(
                    code        = 400,
                    title       = 'Validation Error',
                    description = v.errors
                )

        return data

class StaticResource( object ):
    """
    RESOURCE   : StaticResource
    Description: Current resource is
    used to response with appropriate
    content type due to requested
    file type
    """

    require_auth = False

    def on_get( self, req, resp, file_type, filename ):
        """
        METHOD     : on_get
        Description: Current method allow
        response with appropriate content
        type due to requested file type
        I.E used for static files only
        """

        resp.status  = falcon.HTTP_200
        content_type = os.path.splitext( filename )[1][1:]

        try:

            if content_type == 'js':
                resp.content_type = 'application/javascript'
            elif content_type == 'css':
                resp.content_type = 'text/css'
            elif content_type == 'jpeg' or content_type == 'jpg' or content_type == 'png' :
                resp.content_type = 'image/' + content_type
            elif content_type == 'svg':
                resp.content_type = 'image/svg+xml'

            with open( settings.STATIC_DIR + '/'+ file_type +'/' + filename , 'rb' ) as f:
                resp.body = f.read()

        except:

            resp.content_type = 'text/css'
            resp.body         = ''