import falcon, os, pdfkit, re, json, barcode, bs4, math

from base64 import b64encode, b64decode

from subprocess import call

from pony.orm import *

from .base import BaseResource

from conf import settings

from jinja2 import Environment, FileSystemLoader

class PDFResource( BaseResource ):
    """
    RESOURCE   : PDFResource
    Description: Current resource is
    used to as view of pdf in html
    """

    require_auth = False

    @db_session
    def on_get( self, req, resp ):
        """
        METHOD     : on_get
        Description: Current method
        is used to as view of pdf
        in html
        """

        try:

            pdf_data    = json.load( open( settings.STATIC_DIR + '/uploads/emr-data.json' ) )
            barcode_img = ''

            if pdf_data[ 'barcode' ] and pdf_data[ 'barcode' ] != '' :

                EAN = barcode.get_barcode_class( 'code39' )
                ean = EAN( u' ' + str( pdf_data[ 'barcode' ] ) + '', add_checksum = False )

                ean.save( settings.STATIC_DIR + '/uploads/' + 'barcode_image' )

                barcode_img = '/static/uploads/barcode_image.svg'

            templates_dir = Environment( loader = FileSystemLoader( settings.TEMPLATE_DIR ), trim_blocks = True )

        except Exception:

            raise falcon.HTTPNotFound(
                code        = 400,
                title       = 'Validation Error',
                description = 'Please check your parrams and try again'
            )

        resp.status       = falcon.HTTP_200
        resp.content_type = 'text/html'
        resp.body         = templates_dir.get_template(
            '/conversion/index.html'
        ).render(
            barcode = barcode_img,
            type    = pdf_data[ 'type' ],
            forms   = pdf_data[ 'params' ],
            notes   = pdf_data[ 'notes' ]
        )

class PDFFooterResource( BaseResource ):
    """
    RESOURCE   : PDFFooterResource
    Description: Current resource is
    used to as view of footer in pdf
    in html
    """

    require_auth = False

    @db_session
    def on_get( self, req, resp ):
        """
        METHOD     : on_get
        Description: Current method is
        used to as view of footer in pdf
        in html
        """

        try:

            pdf_data      = json.load( open( settings.STATIC_DIR + '/uploads/emr-data.json' ) )
            templates_dir = Environment(
                loader      = FileSystemLoader(settings.TEMPLATE_DIR),
                trim_blocks = True
            )

        except Exception:

            raise falcon.HTTPNotFound(
                code        = 400,
                title       = 'Validation Error',
                description = 'Please check your parrams and try again'
            )

        resp.status       = falcon.HTTP_200
        resp.content_type = 'text/html'
        resp.body         = templates_dir.get_template(
            '/conversion/pdf-footer.html'
        ).render(
            emr            = pdf_data[ 'emr' ],
            barcode_number = pdf_data[ 'barcode' ]
        )


class PDFHTMLResource( BaseResource ):
    """
    RESOURCE   : PDFHTMLResource
    Description: Current resource is
    used to as view of html of pdf
    """

    require_auth = False

    @db_session
    def on_get( self, req, resp ):
        """
        METHOD     : on_get
        Description: Current method
        is used to as view of html
        of pdf
        """

        try:

            templates_dir = Environment(
                loader = FileSystemLoader( settings.TEMPLATE_DIR ), trim_blocks = True
            )

        except Exception:

            raise falcon.HTTPNotFound(
                code        = 400,
                title       = 'Validation Error',
                description = 'Please check your parrams and try again'
            )

        resp.status       = falcon.HTTP_200
        resp.content_type = 'text/html'
        resp.body         = templates_dir.get_template(
            '/conversion/html-to-pdf-generated.html'
        ).render()


class PDFConvert( BaseResource ):
    """
    RESOURCE   : PDFConvert
    Description: Current resource allow
    pdf converting due to requested data
    """

    validation_scheme = {
        'emr': {
            'type': 'string',
            'required': True
        },
        'type': {
            'required': False
        },
        'barcode': {
            'required': True
        },
        'params': {
            'required': True
        },
        'notes': {
            'required': False
        }
    }

    @db_session
    def on_post( self, req, resp ):
        """
        METHOD     : on_post
        Description: Current method allow
        pdf converting due to requested data
        and return response with file content
        in base 64 format and with appropriate
        message
        """

        data = self.validate( req )

        try:

            replaced_str = dict( ( re.escape( k ), v ) for k, v in { '/convert': '', '/v2': '' }.items() )
            pattern      = re.compile( "|". join( replaced_str.keys() ) )
            pdf_view     = pattern.sub( lambda m: replaced_str[ re.escape( m.group(0 ) ) ], req.url )

            with open( settings.STATIC_DIR + '/uploads/emr-data.json', 'w' ) as outfile:
                json.dump( data, outfile )

            pdfkit.from_url(
                pdf_view,
                settings.STATIC_DIR + '/uploads/pdf_of_emr.pdf',
                options = {
                    'page-size'        : 'A4',
                    'margin-right'     : '0.23in',
                    'margin-left'      : '0.23in',
                    'margin-top'       : '0.23in',
                    'margin-bottom'    : '0.30in',
                    'javascript-delay' : 1000,
                    'footer-html'      : pdf_view + '-footer/',
                    'orientation'      : 'Portrait'
                }
            )

            with open( settings.STATIC_DIR + '/uploads/pdf_of_emr.pdf', "rb" ) as file_pdf_content:
                pdf_content = file_pdf_content.read()

            content_base64_bytes  = b64encode( pdf_content )
            content_base64_string = content_base64_bytes.decode( 'utf-8' )
            os.remove( settings.STATIC_DIR + '/uploads/pdf_of_emr.pdf' )
            os.remove( settings.STATIC_DIR + '/uploads/emr-data.json' )

        except:

            raise falcon.HTTPNotFound(
                code        = 400,
                title       = 'Validation Error',
                description = 'Please check your parrams and try again'
            )

        req.context[ "result" ] = {
            'status' : 'success',
            'code'   : 200,
            'params' : json.dumps ( {
                'file_content' : content_base64_string
            }, indent = 2 )
        }

class PDFHTMLConvert( BaseResource ):
    """
    RESOURCE   : PDFConvert
    Description: Current resource allow
    converting from pdf to html and from
    html to pdf due to requested data
    """

    validation_scheme = {
        'name'    : {
            'required': True
        },
        'content' : {
            'required': True
        },
        'type'    : {
            'required': True
        },
        'notes'   : {
            'required': False
        }

    }

    @db_session
    def on_post( self, req, resp ):
        """
        METHOD     : on_post
        Description: Current method allow
        converting from pdf to html and from
        html to pdf due to requested file
        contend, file name and file type
        return response with html file content
        in base 64 format and with appropriate
        message
        """

        data = self.validate( req )

        try:

            file_name = data[ 'name' ].replace( ' ', '_' )
            file      = open( settings.TEMPLATE_DIR + '/conversion/html-to-pdf-generated.html' if data[ 'type' ] == 'html' else settings.STATIC_DIR + '/uploads/' + file_name + '.' + data[ 'type' ], "wb" )
            file.write( b64decode( data[ 'content' ] ) )
            file.close()

            if ( data[ 'type' ] == 'html' ):

                with open( settings.TEMPLATE_DIR + '/conversion/html-to-pdf-generated.html' ) as inf:
                    txt = inf.read()
                    soup = bs4.BeautifulSoup( txt )

                for jinja_comment in soup.find_all( text=re.compile(r'{#') ):
                    jinja_comment.replace_with( jinja_comment.replace( '{#', '{ #') )

                for page_container in soup.find_all( text = re.compile( r'#page-container{bottom:0;right:0;overflow:auto}' ) ):
                    page_container.replace_with( page_container.replace( '#page-container{bottom:0;right:0;overflow:auto}', '#page-container{bottom:0;right:0;}' ) )

                for script in soup.find_all( 'script' ):
                    script.decompose()

                for pages in soup.find_all( 'div', id = "page-container" ):
                    pages[ "style" ] = "background: none !important;"

                for pages in soup.find_all( 'div', class_ = "pf w0 h0" ):
                    pages[ "style" ] = "box-shadow: none !important; page-break-after: always;"

                if 'notes' in data and data[ 'notes' ] and 'size' in data[ 'notes' ] and data[ 'notes' ][ 'size' ]:

                    original_size              = data[ 'notes' ][ 'size' ][ 'original' ]
                    actual_size                = data[ 'notes' ][ 'size' ][ 'actual' ]
                    scale                      = float( original_size[ 'w' ] )/float( actual_size[ 'w' ] )
                    custom_div_tags            = soup.new_tag( "div", style = "width: " + str( original_size[ 'w' ] ) + "px; height: " + str( original_size[ 'h' ] ) + "px; margin: 0px auto; padding: 0px;" )
                    custom_div_tags[ "class" ] = "container"
                    soup.body.append( custom_div_tags )

                    notes_list            = soup.new_tag( "ul", style = "position: relative; list-style: none; padding-left: 0px; float: left; width: " + str( original_size[ 'w' ] ) + "px; height: " + str( original_size[ 'h' ] ) + "px; margin: 5px 0px; overflow: hidden;" )
                    notes_list[ "class" ] = "notes-list"
                    custom_div_tags.append( notes_list )

                    for note in data[ 'notes' ][ 'items' ]:

                        width  = ( float( note[ 'sizes' ][ 'width' ] ) / float( note[ 'scale' ][ 'x' ] ) )
                        scaleX = ( float( note[ 'scale' ][ 'x' ] ) * scale )
                        scaleY = ( float( note[ 'scale' ][ 'y' ] ) * scale )

                        if ( scaleY  > 10 ) :
                            top = ( float( note[ 'location' ][ 'top' ] ) * scale ) + ( scaleY * 9 )
                        elif ( scaleY > 2 ) :
                            top = ( float( note[ 'location' ][ 'top' ] ) * scale ) + ( scaleY * 3.4 )
                        else:
                            top = ( float( note[ 'location' ][ 'top' ] ) * scale ) - 3

                        original_note_w = float( note[ 'sizes' ][ 'width' ] )
                        left            = ( ( float( note[ 'location' ][ 'left' ] ) + ( ( original_note_w - ( original_note_w / float( note[ 'scale' ][ 'x' ] ) ) )/ 2  ) + ( 41.25/scale ) + 13 ) * scale ) + 10

                        add_note = soup.new_tag( 'li',
                            style = "margin-top:" + str(
                                top ) + "px; margin-left: " + str( left ) + "px; width: " + str( width ) + "px; -moz-transform: rotate(" + str(
                                note[ 'scale' ][ 'angle' ] ) + "deg) scale("+ str( scaleX ) +"," + str( scaleY ) + "); -ms-transform: rotate(" + str(
                                note[ 'scale' ][ 'angle' ] ) + "deg) scale("+ str( scaleX ) +"," + str( scaleY ) + "); -webkit-transform: rotate(" + str(
                                note[ 'scale' ][ 'angle' ] ) + "deg) scale("+ str( scaleX ) +"," + str( scaleY ) + "); -o-transform: rotate(" + str(
                                note[ 'scale' ][ 'angle' ] ) + "deg) scale("+ str( scaleX ) +"," + str( scaleY ) + "); transform: rotate(" + str(
                                note[ 'scale' ][ 'angle' ] ) + "deg) scale("+ str( scaleX ) +"," + str( scaleY ) + ");"
                                "position: absolute; background: #fffddf; color: #000000; text-align: center; font-size: 18px; z-index: 9;"
                        )

                        add_note.string = note[ 'text' ]
                        notes_list.append( add_note )


                with open( settings.TEMPLATE_DIR + '/conversion/html-to-pdf-generated.html', 'w', encoding = 'utf-8' ) as f_out:
                    f_out.write( str( soup ) )

                replaced_str = dict( ( re.escape( k ), v ) for k, v in { '/convert': '', '/v2': '', '/pdf-to-html': '' }.items() )
                pattern      = re.compile( "|".join( replaced_str.keys() ) )
                pdf_view     = pattern.sub( lambda m: replaced_str[ re.escape( m.group( 0 ) ) ], req.url )

                pdfkit.from_url(
                    pdf_view + '/html-of-pdf',
                    settings.STATIC_DIR + '/uploads/' + file_name  + '.pdf',
                    options = {
                        'page-size'     : 'A4',
                        'margin-right'  : '0.23in',
                        'margin-left'   : '0.23in',
                        'margin-top'    : '0.23in',
                        'margin-bottom' : '0.30in',
                        'orientation'   : 'Portrait'
                    }
                )

                with open( settings.STATIC_DIR + '/uploads/' + file_name  + '.pdf', "rb" ) as file_pdf_content:
                    pdf_content = file_pdf_content.read()

                content_base64_bytes = b64encode( pdf_content )
                content_base64_string = content_base64_bytes.decode( 'utf-8' )

            else:

                command      = "pdf2htmlEX --dest-dir " + settings.STATIC_DIR + "/uploads/ --zoom 2.5 " + settings.STATIC_DIR + '/uploads/' + file_name + '.pdf'
                args         = list( command.split( ' ' ) )
                convert_html = call( args )

                if convert_html == 0:

                    with open( settings.STATIC_DIR + '/uploads/' + file_name + '.html' ) as inf:
                        txt = inf.read()
                        soup = bs4.BeautifulSoup( txt )

                    for pages in soup.find_all( 'div', id = "page-container" ):
                        pages[ "style" ] = "background: none !important;"

                    for pc in soup.find_all( text = re.compile( r'pc' ) ):
                        pc.replace_with( pc.replace( 'pc', '' ) )

                    for pages in soup.find_all( 'div', class_ = "pf w0 h0" ):
                        pages[ "style" ] = "box-shadow: none !important;"

                    for sidebar in soup.find_all( 'div', id="sidebar" ):
                        sidebar.decompose()

                    with open( settings.STATIC_DIR + '/uploads/' + file_name + '.html', 'w', encoding = 'utf-8' ) as f_out:
                        f_out.write( str( soup ) )

                    with open( settings.STATIC_DIR + '/uploads/' + file_name + '.html', "rb" ) as file_html_content:
                        pdf_content = file_html_content.read()

                    content_base64_bytes = b64encode( pdf_content )
                    content_base64_string = content_base64_bytes.decode( 'utf-8' )

                else :
                    content_base64_string = ''

                os.remove( settings.STATIC_DIR + '/uploads/' + file_name + '.html' )

            os.remove( settings.STATIC_DIR + '/uploads/' + file_name + '.pdf' )

        except:

            raise falcon.HTTPNotFound(
                code        = 400,
                title       = 'Validation Error',
                description = 'Please check your parrams and try again'
            )

        req.context[ "result" ] = {
            'status' : 'success',
            'code'   : 200,
            'params' : json.dumps ( {
                'file_content' : content_base64_string
            }, indent = 2 )
        }