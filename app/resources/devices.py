import falcon

from pony.orm import *

from .base import BaseResource

from app.models.devices import Devices

class DevicesResource( BaseResource ):
    """
    RESOURCE   : DevicesResource
    Description: Current resource allow
    update device due to requested
    public key
    """

    require_auth      = False
    validation_scheme = {
        'mac_address': {
            'required': True
        },
        'public_key': {
            'type'    : 'string',
            'required': True
        }
    }

    @db_session
    def on_post( self, req, resp ):
        """
        METHOD     : on_post
        Description: Current method allow
        update device with requested parameters
        """

        data = self.validate( req )

        try:

            device = Devices.select( lambda device: device.public_key == data[ 'public_key' ] and device.is_new == 1 ).first()

            if device is not None:

                device.mac_address = data[ 'mac_address' ]
                device.is_new      = 0
                current_device     = Devices[ device.id ]

                current_device.save( data )

            else:

                raise falcon.HTTPNotFound(
                    code        = 400,
                    title       = 'Validation Error',
                    description = 'Please check your parrams and try again'
                )


        except Exception:

            raise falcon.HTTPNotFound(
                code        = 400,
                title       = 'Validation Error',
                description = 'Please check your parrams and try again'
            )

        req.context[ "result" ] = {
            'status' : 'success',
            'code'   : 200,
            'message': 'Device registered successfully'
        }

