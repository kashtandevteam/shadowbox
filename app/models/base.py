import json, arrow

from pony.orm import *
from pony.orm.serialization import json_converter

#Connection to database
db = Database()

class BaseModel:
    """
    MODEL      : BaseModel
    Description: Current model allow
    encoding, saving and serialization
    with requested data
    """

    @classmethod
    @db_session
    def as_json( cls, query ):
        """
        METHOD     : as_json
        Description: Current method allow
        encoding to json format
        """

        entity_list = list()

        for entity in query:

            entity_list.append(
                entity.serialize()
            )

        return json.dumps(
            entity_list,
            default   = json_converter,
            indent    = 2,
            sort_keys = True
        )

    def save( self, args ):
        """
        METHOD     : save
        Description: Current method allow
        fill in created and modified dates
        with appropriate values and save
        all requested data to appropriate
        model
        """

        if hasattr( self, 'created' ) and self.created is None:

            try:
                self.created = arrow.utcnow().date()
            except Exception:
                self.created = arrow.utcnow().datetime

        if hasattr( self, 'modified' ):

            try:
                self.modified = arrow.utcnow().date()
            except Exception:
                self.modified = arrow.utcnow().datetime

        self.set( **args )
        self.flush()

    def serialize( self ):
        """
        METHOD     : serialize
        Description: Serializing response data,
        removing no need elements in response
        """

        return self.to_dict()
