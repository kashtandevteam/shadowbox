import hashlib

from datetime import datetime

from pony.orm import *

from .base import BaseModel, db

from datetime import datetime, timedelta

from app.models.tokens import Tokens

class Devices( BaseModel, db.Entity ):
    """
    MODEL      : Devices
    Description: Current model is
    used to get, insert, update
    data for devices
    """

    id          = PrimaryKey( int, auto=True )
    name        = Optional( str, default = '' )
    mac_address = Required( str )
    public_key  = Required( str )
    private_key = Required( str )
    is_new      = Optional( int, default = 1 )
    created_at  = Optional( datetime, sql_default = 'CURRENT_TIMESTAMP', nullable = True )
    modified_at = Optional( datetime, sql_default = 'CURRENT_TIMESTAMP', nullable = True )

    @db_session
    def serialize( self ):
        """
        METHOD     : serialize
        Description: Serializing response data,
        removing no need elements in response
        """

        return self.to_dict( [
            'id',
            'name',
            'mac_address',
            'public_key',
            'is_blocked'
        ] )

    @classmethod
    def generate_token( cls, args ):
        """
        METHOD     : generate_token
        Description: Current method is used to
        device authorization with help of
        token generation for current device
        due to requested public key and
        mac address.
        """

        current_time   = datetime.now().timestamp()
        current_device = select( device for device in cls
            if device.public_key == args[ 'public_key' ]
                and device.private_key == args[ 'private_key' ]
                and device.mac_address == args[ 'mac_address' ]
        ).first()

        temporary_token = hashlib.sha224(
            ( '%s%s%s%s' % ( args[ 'public_key' ], args[ 'private_key' ], args[ 'mac_address' ], current_time ) ).encode( 'utf-8' )
        ).hexdigest()

        token = Tokens.select(
            lambda tokens: tokens.mac_address == current_device.mac_address
        ).first()

        if token is None:

            token = Tokens(
                mac_address     = args[ 'mac_address' ],
                expiration_date = datetime.now() + timedelta( hours = 10 ),
                temporary_token = temporary_token
            )

        token.save( {
            'expiration_date': datetime.now() + timedelta( hours = 10 ),
            'temporary_token': temporary_token,
            'mac_address'    : args[ 'mac_address' ]
        } )

        return {
            'token': temporary_token,
            'time' : current_time
        }

