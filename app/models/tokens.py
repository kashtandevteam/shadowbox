from datetime import datetime

from pony.orm import *

from .base import BaseModel, db

class Tokens( BaseModel, db.Entity ):
    """
    MODEL      : Tokens
    Description: Current model is
    used to get, insert, update
    generated tokens for auth purposes.
    """

    id              = PrimaryKey( int, auto = True )
    mac_address     = Optional( str, max_len = 25, default = '' )
    temporary_token = Required( str, max_len = 80 )
    expiration_date = Required( datetime )