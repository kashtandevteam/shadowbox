class Singleton( type ):
    """
    UTILS      : Singleton
    Description: Current util provide one and only one
    object of a particular type
    """

    _instances = {}

    def __call__( cls, *args, **kwargs ):
        """
        METHOD     : __call__
        Description: Current util provide one and only one
        object of a particular type
        """

        if cls not in cls._instances:
            cls._instances[ cls ] = super( Singleton, cls ).__call__( *args, **kwargs )

        return cls._instances[cls]
