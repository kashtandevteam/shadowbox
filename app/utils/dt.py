import arrow

def utcnow():
    """
    METHOD     : utcnow
    Description: Current method return
    date of today in timestamp
    """

    return arrow.utcnow().timestamp
