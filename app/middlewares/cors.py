import falcon

from conf import settings

class CorsMiddleware( object ):
    """
    MIDDLEWARE : CorsMiddleware
    Description: Current middleware is
    used to add custom headers to
    response
    """

    def process_response( self, req, resp, resource ):
        """
        METHOD     : process_response
        Description: Current method add
        custom headers to response
        """

        if ',' in settings.CORS_ORIGIN:

            for origin in settings.CORS_ORIGIN.split(','):

                if origin == req.get_header( 'Origin' ):

                    resp.append_header(
                        'Access-Control-Allow-Origin',
                        origin
                    )

                    break

        else:

            resp.append_header(
                'Access-Control-Allow-Origin',
                settings.CORS_ORIGIN
            )

        resp.append_header(
            'Access-Control-Allow-Headers',
            'Authorization, Content-type'
        )

        resp.append_header(
            'Access-Control-Allow-Methods',
            'GET, POST, PATCH, DELETE'
        )

        if req.method == 'OPTIONS':
            resp.status = falcon.HTTP_200
            return
