import falcon, json

from pony.orm.core import QueryResult, Query
from pony.orm.serialization import json_converter

from app.models.base import BaseModel

class RequireJSON( object ):
    """
    MIDDLEWARE : RequireJSON
    Description: Current middleware
    is used to check request content
    type in header
    """

    def process_request( self, req, resp ):
        """
        METHOD     : process_request
        Description: Current method is
        used to check content type in
        header requests.
        I.E checks json valid and returns
        appropriate response
        """

        if not req.client_accepts_json and req.accept != 'text/html':

            raise falcon.HTTPNotAcceptable(
                code        = 400,
                title       = 'Requested Header Error',
                description = 'This API only supports responses encoded as JSON.'
            )

class JSONTranslator( object ):
    """
    MIDDLEWARE : JSONTranslator
    Description: Current middleware is
    used to encode and decode to json
    format.
    Also checks body params format and
    return appropriate response
    """

    def process_request( self, req, resp ):
        """
        METHOD     : process_request
        Description: Current method allow
        checking valid json format in request
        and return appropriate response
        """

        if req.content_length in ( None, 0 ):
            return

        try:

            if 'application/json' in req.content_type:

                body = req.stream.read()

                if not body:

                    raise falcon.HTTPBadRequest(
                        code = 400,
                        title = 'Empty request body',
                        description = 'A valid JSON document is required.'
                    )

                req.context[ 'body' ] = body

                req.context['json'] = json.loads(
                    body.decode( 'utf-8' )
                )

        except ( ValueError, UnicodeDecodeError ):

            raise falcon.HTTPBadRequest(
                code        = 400,
                title       = falcon.HTTP_753,
                description = 'Malformed JSON'
                              'Could not decode the request body. The '
                              'JSON was incorrect or not encoded as '
                              'UTF-8.'
            )

    def process_response( self, req, resp, resource ):
        """
        METHOD     : process_response
        Description: Current method allow
        encode data to json and return it
        in json format
        """

        if 'result' in req.context:

            if isinstance( req.context['result'], QueryResult ) or isinstance( req.context['result'], Query ):

                resp.body = BaseModel.as_json(
                    req.context['result']
                )

            else:

                resp.body = json.dumps(
                    req.context[ 'result' ],
                    default   = json_converter,
                    indent    = 2,
                    sort_keys = True
                )
