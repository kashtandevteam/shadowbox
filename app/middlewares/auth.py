import falcon

from pony.converting import ValidationError
from pony.orm import db_session

from datetime import datetime

from app.models.devices import Devices
from app.models.tokens import Tokens

class AuthMiddleware(object):
    """
    MIDDLEWARE : AuthMiddleware
    Description: Current middleware is
    used to check authorization token
    for all requests in header
    """

    @db_session
    def process_request( self, req, resp ) :
        """
        METHOD     : process_request
        Description: Current method checks
        the param existence in header and
        returns appropriate response
        """

        auth_header = req.get_header(
            'Authorization'
        )

        if auth_header:

            try:
                auth_type, token = auth_header.split(' ')
            except ValidationError:

                raise falcon.HTTPUnauthorized(
                    code        = 401,
                    title       = 'Authentication failed',
                    description = 'Token parameter is incorrect.'
                )

            if auth_type.lower() != 'token':

                raise falcon.HTTPUnauthorized(
                    code        = 401,
                    title       = 'Authentication failed',
                    description = 'Invalid authentication type.'
                )

            if not token:

                raise falcon.HTTPUnauthorized(
                    code        = 401,
                    title       = 'Authentication failed',
                    description = 'No token provided. Unable to authorize current device.'
                )

            try:

                token = Tokens.select(
                    lambda tokens: tokens.temporary_token == token
                ).first()

                expiration   = token.expiration_date
                current_time = datetime.utcnow()

                if ( current_time > expiration ) :

                    raise falcon.HTTPUnauthorized(
                        code        = 401,
                        title       = 'Authentication failed',
                        description = 'Your session expired. Please re-login again '
                    )

            except Exception:

                raise falcon.HTTPUnauthorized(
                    code        = 401,
                    title       = 'Authentication failed',
                    description = 'Your session expired. Please re-login again '
                )

            if token is None:

                raise falcon.HTTPUnauthorized(
                    code        = 401,
                    title       = 'Authentication failed',
                    description = 'The provided auth token is not valid. Please request a new token and try again.'
                )

            req.context[ 'device' ] = Devices.select(
                lambda device: device.mac_address == token.mac_address
            ).first()

    def process_resource( self, req, resp, resource ):
        """
        METHOD     : process_resource
        Description: Current method allow
        save data for current session for
        further usage ( data of authorized device )
        """

        if resource:
            resource.device = req.context.pop(
                'device',
                None
            )

        if resource and resource.require_auth:
            resource.check_auth( req )
