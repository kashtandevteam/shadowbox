# Main route file. Allowing connection for custom tables and get or update information

import falcon

# SQL connection to database
from pony.orm import set_sql_debug

# Import settings from settings files
from conf import settings

# Middlewares allowing checking authorization token and getting device information according to requested token
from .middlewares.auth import AuthMiddleware
from .middlewares.json import JSONTranslator, RequireJSON
from .middlewares.cors import CorsMiddleware

# The list of available resources
from .resources.auth import AuthResource, LogoutResource
from .resources.devices import DevicesResource
from .resources.hl7 import HL7Convert
from .resources.pdf import PDFResource, PDFFooterResource, PDFConvert, PDFHTMLConvert, PDFHTMLResource
from .resources.base import StaticResource

# The list of available database tables
from .models.base import db
from .models.devices import Devices
from .models.tokens import Tokens

# Database Connection
db.bind(
    provider = settings.DB_TYPE,
    host     = settings.DB_HOST,
    user     = settings.DB_USER,
    passwd   = settings.DB_PASSWORD,
    db       = settings.DB_NAME,
    ssl      = settings.SSL_OPTIONS
)

db.generate_mapping(
    create_tables = False
)

set_sql_debug( True )

def create_app( debug = False ):
    """
    METHOD     : create_app
    Description: Current method is
    used to create base routes and
    connect it with models and
    resources respectively
    """

    app = falcon.API( middleware = [
        RequireJSON(),
        JSONTranslator(),
        AuthMiddleware(),
        CorsMiddleware(),
    ] )

    base_uri = '/' + settings.API_VERSION + '/'

    # Auth
    app.add_route( base_uri + 'auth', AuthResource() )

    # New device
    app.add_route( base_uri + 'device/add', DevicesResource() )

    # PDF conversion
    app.add_route( base_uri + 'pdf/convert/', PDFConvert() )

    # PDF TO HTML conversion
    app.add_route( base_uri + 'pdf-to-html/convert/', PDFHTMLConvert() )

    # HL7 conversion
    app.add_route( base_uri + 'hl7/convert/', HL7Convert() )

    # HL7 TO PDF conversion
    #app.add_route( base_uri + 'hl7/convert/', HL7Convert() )

    # Log Out
    app.add_route( base_uri + 'logout', LogoutResource() )

    # PDF view
    app.add_route( '/pdf', PDFResource() )

    # PDF view for footer
    app.add_route( '/pdf-footer/', PDFFooterResource() )

    # HTML PDF view
    app.add_route( '/html-of-pdf', PDFHTMLResource() )

    # Styles/Images and Files main route
    app.add_route( '/static/{file_type}/{filename}', StaticResource() )

    return app