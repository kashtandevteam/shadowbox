from wsgiref import simple_server
from app.app import create_app

debug = False
if __name__ == '__main__':
    debug = True

app = application = create_app( debug )

if debug:
    httpd = simple_server.make_server( '0.0.0.0', 8001, app )
    httpd.serve_forever()
